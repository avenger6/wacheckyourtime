import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TimerDeclarations } from './shared/declarations/timer.declarations';
import { StorageService } from './shared/services/storage.service';

@NgModule({
  declarations: [AppComponent, ...TimerDeclarations],
  imports: [BrowserModule, ReactiveFormsModule],
  providers: [StorageService],
  bootstrap: [AppComponent]
})
export class AppModule {}
