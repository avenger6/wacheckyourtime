export class Task {
  name: string = null;
  description: string = null;
  created_date: string = null;
  start_date: string = null;
  duration: number = 0;

  constructor(fields?: any) {
    if (fields) {
      Object.assign(this, fields);
    }
  }
}
